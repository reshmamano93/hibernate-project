package com.reshma.hibernate.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class SingletonSessionFactory {

    private static SessionFactory factory;

    private SingletonSessionFactory() {
        super();
    }

    public static SessionFactory getSessionFactory() {
        if (factory == null) {
            Configuration configuration = new Configuration();
            configuration.configure();
            factory = configuration.buildSessionFactory();
        }
        return factory;
    }
}
