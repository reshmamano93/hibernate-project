package com.reshma.hibernate.utils;

import com.reshma.hibernate.dao.LaptopDAOBuiltIn;
import com.reshma.hibernate.dao.LaptopDAOHql;
import com.reshma.hibernate.dto.LaptopDTO;

import java.util.List;

public class MainProgram {

    public static void main(String[] args) {

        //Built in methods

        //Create
        LaptopDTO laptopDTO = new LaptopDTO();
        laptopDTO.setName("Lenovo");
        laptopDTO.setSpecifications("Intel i7 7th Gen");
        laptopDTO.setPrice(59999D);
        laptopDTO.setUserRatings(4.8D);
        LaptopDAOBuiltIn laptopDAOBuiltIn = new LaptopDAOBuiltIn();
        laptopDAOBuiltIn.saveLaptopDetails(laptopDTO);

        LaptopDTO laptopDTO2 = new LaptopDTO();
        laptopDTO2.setName("HP");
        laptopDTO2.setSpecifications("Intel i5 5th Gen");
        laptopDTO2.setPrice(45999D);
        laptopDTO2.setUserRatings(4.5D);
        laptopDAOBuiltIn.saveLaptopDetails(laptopDTO2);

        LaptopDTO laptopDTO3 = new LaptopDTO();
        laptopDTO3.setName("Acer");
        laptopDTO3.setSpecifications("Intel i3 5th Gen");
        laptopDTO3.setPrice(29999D);
        laptopDTO3.setUserRatings(3.8D);
        laptopDAOBuiltIn.saveLaptopDetails(laptopDTO3);

        //Read
        LaptopDTO details = laptopDAOBuiltIn.getLaptopDetailsById(1L);
        System.out.println(details);

        //Update
        laptopDAOBuiltIn.updateLaptopPriceById(1L, 55999D);

        //Delete
        laptopDAOBuiltIn.deleteLaptop(laptopDTO);

        //Using HQL

        //Read
        LaptopDAOHql laptopDAOHql = new LaptopDAOHql();
        List<LaptopDTO> laptopDetails = laptopDAOHql.getLaptopDetails();
        for (LaptopDTO d : laptopDetails) {
            System.out.println(d);
        }

        LaptopDTO hp = laptopDAOHql.getLaptopDetailsByName("HP");
        System.out.println(hp);

        //Update
        laptopDAOHql.updatePriceByLaptopName("HP", 39999D);

        //Delete
        laptopDAOHql.deleteById(2L);

        List<LaptopDTO> laptopDetails2 = laptopDAOHql.getLaptopDetails();
        for (LaptopDTO d : laptopDetails2) {
            System.out.println(d);
        }
    }
}
