package com.reshma.hibernate.dto;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "laptop")
public class LaptopDTO implements Serializable {

    @Id
    @GenericGenerator(name = "model_auto", strategy = "increment")
    @GeneratedValue(generator = "model_auto")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;

    @Column(name = "specifications")
    private String specifications;

    @Column(name = "user_ratings")
    private Double userRatings;

    public LaptopDTO() {
        // TODO Auto-generated constructor stub
    }

    public LaptopDTO(String name, Double price, String specifications, Double userRatings) {
        super();
        this.name = name;
        this.price = price;
        this.specifications = specifications;
        this.userRatings = userRatings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public Double getUserRatings() {
        return userRatings;
    }

    public void setUserRatings(Double userRatings) {
        this.userRatings = userRatings;
    }

    @Override
    public String toString() {
        return "LaptopDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", specifications='" + specifications + '\'' +
                ", userRatings=" + userRatings +
                '}';
    }
}

