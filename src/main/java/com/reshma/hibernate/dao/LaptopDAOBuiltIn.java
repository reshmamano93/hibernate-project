package com.reshma.hibernate.dao;

import com.reshma.hibernate.dto.LaptopDTO;
import com.reshma.hibernate.utils.SingletonSessionFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class LaptopDAOBuiltIn {

    public void saveLaptopDetails(LaptopDTO laptopDTO) {
        SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(laptopDTO);
        transaction.commit();
        System.out.println("Create successful");
    }

    public LaptopDTO getLaptopDetailsById(Long id) {
        SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        System.out.println("Read successful");
        return session.get(LaptopDTO.class, id);
    }

    public void updateLaptopPriceById(Long id, Double price) {
        LaptopDTO laptopDTO = getLaptopDetailsById(id);
        if (laptopDTO != null) {
            SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            laptopDTO.setPrice(price);
            session.update(laptopDTO);
            transaction.commit();
            System.out.println("Update successful");
        }
    }

    public void deleteLaptop(LaptopDTO laptopDTO) {
        SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(laptopDTO);
        transaction.commit();
        System.out.println("Delete successful");
    }
}
