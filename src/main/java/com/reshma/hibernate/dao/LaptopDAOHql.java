package com.reshma.hibernate.dao;

import com.reshma.hibernate.dto.LaptopDTO;
import com.reshma.hibernate.utils.SingletonSessionFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;


public class LaptopDAOHql {

    public List<LaptopDTO> getLaptopDetails() {
        SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "from LaptopDTO";
        Query query = session.createQuery(hql);
        List<LaptopDTO> list = query.list();
        return list;
    }

    public LaptopDTO getLaptopDetailsByName(String laptopName) {
        SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "from LaptopDTO where name=:laptopName";
        Query query = session.createQuery(hql);
        query.setParameter("laptopName", laptopName);
        LaptopDTO uniqueResult = (LaptopDTO) query.uniqueResult();
        return uniqueResult;
    }

    public void updatePriceByLaptopName(String name, double newPrice) {
        SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        String hql = "update LaptopDTO set price=:newPrice where name=:name";
        Query query = session.createQuery(hql);
        query.setParameter("newPrice", newPrice);
        query.setParameter("name", name);
        int updateRows = query.executeUpdate();
        transaction.commit();
        if (updateRows == 0) {
            System.out.println("Update Operation Failed");
            return;
        }
        System.out.println("Update Operation successful");
    }

    public void deleteById(Long id) {
        SessionFactory sessionFactory = SingletonSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        String hql = "delete LaptopDTO where id=:id";
        Query query = session.createQuery(hql);
        query.setParameter("id", id);
        int updateRows = query.executeUpdate();
        transaction.commit();
        if (updateRows == 0) {
            System.out.println("Delete Operation Failed");
            return;
        }
        System.out.println("Delete Operation successful");
    }
}
